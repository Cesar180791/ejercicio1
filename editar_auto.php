<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" />
  <title>Editoriales</title>
</head>

<body>
  <br />
  <?php
    include './conexion.php';
    $conn = OpenCon();
    $sql = "SELECT * FROM auto INNER JOIN marcas ON auto.idMarca = marcas.idMarca WHERE id = '".$_GET['codigo']."'";

    $sql2 = "SELECT * FROM marcas";

    foreach($conn->query($sql) as $row) {
    ?>
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h4>Editar Auto</h4>
      </div>
      <div class="card-body">
        <form action="" method="POST">
          <label>Marca:</label>
          <select name="marca" id="marca" class="form-control">
            <option value="<?php echo $row['idMarca'] ?>" ><?php echo $row['nombreMarca'] ?></option>
            <?php 
                foreach($conn->query($sql2) as $row2) {
                  echo "<option value=".$row2["idMarca"].">".$row2["nombreMarca"]. "</option>";
                } 
            ?>
          </select>
          <br />
          <label>Nombre Auto:</label>
          <input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo $row['nombre'] ?>" />
          <br />
          <label>Descripcion:</label>
          <input type="text" name="descripcion" id="descripcion" class="form-control" value="<?php echo $row['descripcion'] ?>"/>
          <br />
          <label>Tipo de Combustible:</label>
          <input type="text" name="tipoCombustible" id="tipoCombustible" class="form-control" value="<?php echo $row['tipoCombustible'] ?>"/>
          <br />
          <label>Numero de Puertas:</label>
          <input type="number" name="cantidadPuertas" id="cantidadPuertas" class="form-control" value="<?php echo $row['cantidadPuertas'] ?>"/>
          <br />
          <label>Precio:</label>
          <input type="text" name="precio" id="precio" class="form-control" value="<?php echo $row['precio'] ?>"/>
          <br />
          <input type="Submit" value="Guardar" name="submit" class="btn btn-success" />
          <a class="btn btn-info" href="listar_auto.php">Regresar</a>
          <br />

        </form>
      </div>
    </div>
  </div>

  <?php
    }
         if(isset($_POST["submit"])){
            
           
            // Verificamos la conexión
            if ($conn == null) {
               die("No se pudo conectar a la base de datos: ");
            } 
            $sql = "UPDATE auto SET idMarca ='".$_POST['marca']."', nombre ='".$_POST['nombre']."' , descripcion ='".$_POST['descripcion']."'
            , tipoCombustible ='".$_POST['tipoCombustible']."', cantidadPuertas ='".$_POST['cantidadPuertas']."', precio ='".$_POST['precio']."'
             WHERE id ='".$_GET['codigo']."'";
            
            $count = $conn->exec($sql);

            if ($count > 0) {
               echo "<div class=\"alert alert-success\" role=\"alert\">";
               echo "Se ha editado el Auto";
               echo "</div>";
            } else {
               echo "<div class=\"alert alert-danger\" role=\"alert\">";
               echo "No se pudo editar el auto. ";
               echo "Error: " . $sql;
               print_r($conn->errorInfo());
               echo "</div>";               
            }
            CloseCon($conn);
         }
        
      ?>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
  </script>
</body>

</html>
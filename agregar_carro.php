<?php 
include 'conexion.php';
include 'C:/laragon/www/ejercicio1/navbar.php';
$conn = OpenCon();
           
// Verificamos la conexión
if ($conn == null) {
   die("No se pudo conectar a la base de datos: ");
} 
//Consultas para llenar select de marca
$sql = "SELECT * FROM marcas";
?>
<main class="page-content">
   <div class="container-fluid">
      <div class="row">
         <div class="form-group col-md-12">
            <div class="card">
               <div class="card-header">
                  <h4>Agregar Carro</h4>
               </div>
               <div class="card-body">
                  <form action="" method="POST">
                     <label>Marca:</label>
                     <select name="marca" id="marca" class="form-control">
                        <option>Seleccione una marca</option>
                        <?php 
                        foreach($conn->query($sql) as $row) {
                           echo "<option value=".$row["idMarca"].">".$row["nombreMarca"]. "</option>";
                        } 
                        ?>
                     </select>
                     <br />
                     <label>Nombre Auto:</label>
                     <input type="text" name="nombre" id="nombre" class="form-control" />
                     <br />
                     <label>Descripcion:</label>
                     <input type="text" name="descripcion" id="descripcion" class="form-control" />
                     <br />
                     <label>Tipo de Combustible:</label>
                     <input type="text" name="tipoCombustible" id="tipoCombustible" class="form-control" />
                     <br />
                     <label>Numero de Puertas:</label>
                     <input type="number" name="cantidadPuertas" id="cantidadPuertas" class="form-control" />
                     <br />
                     <label>Precio:</label>
                     <input type="text" name="precio" id="precio" class="form-control" />
                     <br />
                     <input type="Submit" value="Guardar" name="submit" class="btn btn-success" />
                     <a class="btn btn-info" href="listar_auto.php">Regresar</a>
                     <br />
                  </form>
               </div>
            </div>


            <?php
         if(isset($_POST["submit"])){
       
            $sql = "INSERT INTO auto(idMarca, nombre, descripcion, tipoCombustible, cantidadPuertas, precio)
            VALUES ('".$_POST["marca"]."','".$_POST["nombre"]."','".$_POST["descripcion"]."','".$_POST["tipoCombustible"]."','".$_POST["cantidadPuertas"]."','".$_POST["precio"]."')";
            
            $count = $conn->exec($sql);

            if ($count > 0) {
               echo "<div class=\"alert alert-success\" role=\"alert\">";
               echo "Se ha guardado el auto";
               echo "</div>";
            } else {
               echo "<div class=\"alert alert-danger\" role=\"alert\">";
               echo "No se pudo guardar el auto. ";
               echo "Error: " . $sql;
               print_r($conn->errorInfo());
               echo "</div>";               
            }
            CloseCon($conn);
         }
      ?>

         </div>
      </div>

      <footer class="text-center">
         <div class="mb-2">
            <small>
               © 2021 made with <i class="fa fa-heart" style="color:red"></i> by - <a target="_blank"
                  rel="noopener noreferrer" href="#">
                  Cesar Rivera
               </a>
            </small>
         </div>
      </footer>
   </div>
</main>
<!-- page-content" -->
</div>
<!-- page-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
   integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
   integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="./js/js.js"></script>

</body>

</html>
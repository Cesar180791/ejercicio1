<?php
function OpenCon()
 {
    $dsn = 'mysql:dbname=ejercicio1;host=127.0.0.1';
    $usuario = 'root';
    $contraseña = '';

    try {
       $mbd = new PDO($dsn, $usuario, $contraseña);
    }
    catch (PDOException $e) {
       die("Fallo la conexion: " . $e->getMessage());
       $mbd = null;
    }
    return $mbd;

   }

   function CloseCon($mbd) {
      $mbd = null;
   }
 
?>
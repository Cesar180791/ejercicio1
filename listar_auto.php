
<?php 
include 'C:/laragon/www/ejercicio1/navbar.php';
?>
<main class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="form-group col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-10">
                        <h4>Lista de Autos</h4>
                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-success" href="agregar_carro.php"><i class="fas fa-plus"></i> Agregar Auto</a>
                    </div>
                </div>                
            </div>

            <?php
            include './conexion.php';
            $conn = OpenCon();
            
            $sql = "SELECT * FROM auto INNER JOIN marcas ON auto.idMarca = marcas.idMarca";
                ?>

                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Marca</th>
                            <th>Auto</th>
                            <th>Descripcion</th>
                            <th>Tipo Combustible</th>
                            <th>N Puertas</th>
                            <th>Precio</th>
                            <th>Opciones</th>
                        </tr>                        
                    </thead>
                    <tbody>
                <?php
                    foreach($conn->query($sql) as $row) {
                        echo "<tr>";
                        echo "<td>" . $row["id"]. "</td>";
                        echo "<td>" . $row["nombreMarca"]. "</td>";
                        echo "<td>" . $row["nombre"]. "</td>";
                        echo "<td>" . $row["descripcion"]. "</td>";
                        echo "<td>" . $row["tipoCombustible"]. "</td>";
                        echo "<td>" . $row["cantidadPuertas"]. "</td>";
                        echo "<td>" . $row["precio"]. "</td>";
                        echo "<td>";
                        echo "<a class=\"btn btn-primary\"href=\"http://localhost/ejercicio1/ver_auto.php?codigo=". $row["id"]."\"><i class=\"fas fa-eye\"></i></a> ";
                        echo "<a class=\"btn btn-warning\"href=\"http://localhost/ejercicio1/editar_auto.php?codigo=". $row["id"]."\"><i class=\"fas fa-edit\"></i></a> ";
                        echo "<a class=\"btn btn-danger\" href=\"http://localhost/ejercicio1/eliminar_auto.php?codigo=". $row["id"]."\"><i class=\"far fa-trash-alt\"></i></a>";
                        echo "</td>";
                        echo "</tr>";
                    }

                    ?>
                    </tbody>
                </table>
                <?php

                CloseCon($conn);
                ?>
                    
                </div>

        <?php
            if (isset($_GET['result'])) {
               if($_GET['result'] == 1) {
                    echo "<div class=\"alert alert-success\" role=\"alert\">";
                    echo "Se ha eliminado el libro";
                    echo "</div>";
               }else{
                    echo "<div class=\"alert alert-danger\" role=\"alert\">";
                    echo "No se pudo eliminar el libro. ";                
                    echo "</div>";  
               }
            }
        ?>
        </div>
      </div>

      <footer class="text-center">
        <div class="mb-2">
          <small>
            © 2021 made with <i class="fa fa-heart" style="color:red"></i> by - <a target="_blank" rel="noopener noreferrer" href="#">
              Cesar Rivera
            </a>
          </small>
        </div>
      </footer>
    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="./js/js.js"></script>
    
</body>

</html>
